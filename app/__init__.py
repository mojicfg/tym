from flask import Flask 
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
import os

app = Flask(__name__.split('.')[0])
app.config.update({
    'DEV_CONTACT': os.environ.get('FLASK_DEV'),
    'SECRET_KEY': os.environ.get('FLASK_SECRET'),
    'SQLALCHEMY_DATABASE_URI': 'sqlite:///data.db',
    'SQLALCHEMY_TRACK_MODIFICATIONS': False
    })
bc = Bcrypt(app)
db = SQLAlchemy(app)

from .routes import *


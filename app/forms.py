from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, SelectField, StringField, SubmitField
from wtforms.validators import DataRequired, EqualTo, Length, ValidationError
from . import bc
from .models import NAME_LEN, PW_LEN, Problem, User

valid_chars = 'абвгґдеєжзиіїйклмнопрстуфхцчшщьюяАБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ \''

def valid_str(form, field):
    if any(not c in valid_chars for c in field.data):
        raise ValidationError('Використано заборонені символи.')

class RegistrationForm(FlaskForm):
    username = StringField('Ім\'я учасника', validators=[
        DataRequired(),
        valid_str,
        Length(min=2, max=NAME_LEN, message='Уведено занадто коротке/довге ім\'я.')
        ])
    password = PasswordField('Секретний ключ', validators=[
        DataRequired(),
        Length(min=1, max=PW_LEN, message='Уведено занадто короткий/довгий ключ.')
        ])
    confirm = PasswordField('Повторіть ключ', validators=[
        DataRequired(),
        EqualTo('password', message='Уведені ключі не збігаються.')
        ])
    submit = SubmitField('Надіслати')

    def validate_username(form, field):
        user = User.query.filter_by(name=field.data).first()
        if user:
            raise ValidationError('Учасника з таким ім\'ям уже зареєстровано.')

class SubmissionForm(FlaskForm):
    problem = SelectField('Задача')
    state = SelectField('Стан')
    generalized = BooleanField('Узаг-ня')
    report_ready = BooleanField('Доповідь')
    username = StringField('Ім\'я', validators=[
        DataRequired(),
        valid_str,
        Length(min=2, max=NAME_LEN, message='Уведено занадто коротке/довге ім\'я.')
        ])
    password = PasswordField('Ключ', validators=[
        DataRequired(),
        Length(min=1, max=PW_LEN, message='Уведено занадто короткий/довгий ключ.')
        ])
    submit = SubmitField('Надіслати')

    def validate_problem(form, field):
        prob = Problem.query.filter_by(number=field.data).first()
        if prob is None:
            raise ValidationError('Обрано некоректний номер завдання.')
        if prob.user and prob.user.name != form.username.data:
            raise ValidationError(f'{prob.user.name} уже розв\'язує це завдання.')

    def validate_username(form, field):
        user = User.query.filter_by(name=field.data).first()
        if user is None:
            raise ValidationError('Учасника з таким ім\'ям не знайдено.')
        if not user.approved:
            raise ValidationError('Цього учасника ще не зареєстровано.')

    def validate_password(form, field):
        user = User.query.filter_by(name=form.username.data).first()
        if user and not bc.check_password_hash(user.pw_hash, field.data):
            raise ValidationError('Уведено неправильний ключ.')


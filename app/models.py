from . import db

NUM_LEN = 4
NAME_LEN = 16
PW_LEN = 16
PW_HASH_LEN = 60

problem_states = {
        0: 'Не розв\'язано',
        1: 'Частково',
        2: 'Розв\'язано',
        3: 'Прийнято'
        }
problem_state_colours = {
        0: "#bb0000",
        1: "#ddaa00",
        2: "#11bb11",
        3: "#009900"
        }

class Problem(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    number = db.Column(db.String(NUM_LEN), unique=True, nullable=False)
    state = db.Column(db.Integer, default=0, nullable=False)
    generalized = db.Column(db.Boolean, default=False, nullable=False)
    report_ready = db.Column(db.Boolean, default=False, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('problems', lazy=True))

    def __repr__(self):
        return f'<Problem {self.number}>'

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(NAME_LEN), unique=True, nullable=False)
    pw_hash = db.Column(db.String(PW_HASH_LEN), nullable=False)
    approved = db.Column(db.Boolean, default=False, nullable=False)

    def __repr__(self):
        return f'<User {self.name}>'


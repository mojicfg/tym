from flask import redirect, render_template, url_for
from . import app, bc, db
from .forms import RegistrationForm, SubmissionForm
from .models import Problem, problem_states, problem_state_colours, User

@app.route("/")
@app.route("/status")
def status():
    return render_template('status.html', title='Статус',
            problems=[
                {
                    'number': problem.number,
                    'state': problem.state,
                    'generalized': problem.generalized,
                    'report_ready': problem.report_ready,
                    'username': problem.user.name if problem.user else None,
                    }
                for problem in Problem.query],
            problem_states=problem_states,
            problem_state_colours=problem_state_colours,
            dev_contact=app.config.get('DEV_CONTACT'))

@app.route("/submit", methods=['GET', 'POST'])
def submit():
    form = SubmissionForm()
    form.problem.choices = [(problem.number, problem.number) for problem in Problem.query]
    form.state.choices = [(str(key), val) for key, val in problem_states.items()]
    if form.validate_on_submit():
        prob = Problem.query.filter_by(number=form.problem.data).first()
        user = User.query.filter_by(name=form.username.data).first()
        prob.user = user
        prob.state = int(form.state.data)
        prob.generalized = bool(form.generalized.data)
        prob.report_ready = bool(form.report_ready.data)
        db.session.commit()
        return redirect(url_for('status'))
    return render_template('submit.html', title='Додати', form=form)

@app.route("/stats")
def stats():
    return render_template('stats.html', title='Статистика')

@app.route("/login")
def login():
    return render_template('login.html', title='Увійти')

@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        pw_hash = bc.generate_password_hash(form.password.data).decode('utf-8')
        db.session.add(User(name=form.username.data, pw_hash=pw_hash))
        db.session.commit()
        return redirect(url_for('status'))
    return render_template('register.html', title='Реєстрація', form=form)

